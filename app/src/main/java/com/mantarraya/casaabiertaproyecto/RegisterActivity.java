package com.mantarraya.casaabiertaproyecto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    Button btn_register;
    TextInputLayout full_name, ci_document, username, email, password;
    //EditText full_name, ci_document, username, email, password;

    FirebaseFirestore mFirestore;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.setTitle("Registro");

        //firebase
        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        //campos de formulario
        full_name = findViewById(R.id.full_name);
        ci_document = findViewById(R.id.ci_document);
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        //botones
        //btn_regresar = findViewById(R.id.btn_register);
        btn_register = findViewById(R.id.btn_register);



        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String full_name_User = full_name.getEditText().toString().trim();
                String ci_document_User = ci_document.getEditText().toString().trim();
                String username_User = username.getEditText().toString().trim();
                String email_User = email.getEditText().toString().trim();
                String password_User = password.getEditText().toString().trim();

                if(full_name_User.isEmpty() && ci_document_User.isEmpty() && username_User.isEmpty() && email_User.isEmpty() && password_User.isEmpty()){

                    Toast.makeText(RegisterActivity.this, "Complete los datos", Toast.LENGTH_SHORT).show();

                }else{
                    registerUser(full_name_User, ci_document_User, username_User, email_User, password_User);

                }


            }
        });
    }

    public void Regresar(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



    private void registerUser(String full_name_user, String ci_document_user, String username_user, String email_user, String password_user) {
        mAuth.createUserWithEmailAndPassword(email_user, password_user).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                String id = mAuth.getCurrentUser().getUid();

                Map<String, Object> map = new HashMap<>();
                map.put("id", id);
                map.put("full_name", full_name_user);
                map.put("ci_document", ci_document_user);
                map.put("username", username_user);
                map.put("email", email_user);
                map.put("password",password_user);

                mFirestore.collection("user").document(id).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        finish();
                        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                        Toast.makeText(RegisterActivity.this, "Usuario registrado exitosamente", Toast.LENGTH_SHORT).show();
                        Log.e("ErrorAdd", "registrado exitosamente de usuario");

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RegisterActivity.this, "Error al guardar", Toast.LENGTH_SHORT).show();
                        Log.e("ErrorAdd", "Error al guardar usuario");
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(RegisterActivity.this, "Error al registrarte", Toast.LENGTH_SHORT).show();
                Log.e("ErrorAdd", "Error al registrar usuario");
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}