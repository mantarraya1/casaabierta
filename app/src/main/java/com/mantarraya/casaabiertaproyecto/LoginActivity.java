package com.mantarraya.casaabiertaproyecto;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginActivity extends AppCompatActivity {

    Button btn_login, btn_login_anonymous;
    EditText email, password;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        btn_login= findViewById(R.id.ingresar);
        btn_login_anonymous = findViewById(R.id.btn_anonymous);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email_User = email.getText().toString().trim();
                String password_User = password.getText().toString().trim();

                if (email_User.isEmpty() && password_User.isEmpty()){
                    Toast.makeText(LoginActivity.this,"Ingreso de datos", Toast.LENGTH_LONG).show();
                    Log.e("Login", "Ingreso exitoso");
                }else{
                    loginUser(email_User,password_User);
                }
            }
        });
        btn_login_anonymous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loginanonymous();
            }
        });
    }
    private void loginanonymous(){
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception a){
                Toast.makeText(LoginActivity.this, "Error al acceder", Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void loginUser(String email_User, String password_User){
        mAuth.signInWithEmailAndPassword(email_User, password_User).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    finish();
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    Toast.makeText(LoginActivity.this, "Error",Toast.LENGTH_LONG).show();
                    Log.e("Login", "Error al ingreso");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override


            public void onFailure(@NonNull Exception e) {
                Toast.makeText(LoginActivity.this, "Error al iniciar",Toast.LENGTH_LONG).show();
                Log.e("Login", "Error al iniciar");
            }
        });
    }
}
